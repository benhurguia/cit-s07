-- 3.) ADD THE FOLLOWING RECORDS TO THE blog_db DATABASE:

--     USERS:
--         John Smith
--             INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com","passwordA","2021-01-01 01:00:00");
        
--         Juan Dela Cruz
--             INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com","passwordB","2021-01-01 02:00:00");
        
--         Jane Smith
--             INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com","passwordC","2021-01-01 03:00:00");
        
--         Maria Dela Cruz
--             INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD","2021-01-01 04:00:00");
        
--         John Doe
--             INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com","passwordD","2021-01-01 05:00:00");
    

--     POSTS:
--         User ID 1
--             INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(1,"First Code","Hello World!","2021-01-02 01:00:00");
--             INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(1,"Second Code","Hello Earth!","2021-01-02 02:00:00");

--         User ID 2
--             INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(2,"Third Code","Welcome to Mars!","2021-01-02 03:00:00");

--         User ID 4
--             INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(4"Fourth Code","Bye bye solar system!","2021-01-02 04:00:00");


-- 4.) GET ALL THE POST WITH AN AUTHOR ID OF 1:
--     SELECT * from posts WHERE user_id = 1;


-- 5.) GET ALL THE USER'S EMAIL AND DATETIME OF CREATION:
--     SELECT email, datetime_created FROM users;

-- 6.) UPDATE A POST'S CONTENT TO HELLO TO THE PEOPLE OF THE EARTH! WHERE ITS INITIAL CONTENT IS HELLO EARTH! BY USING THE RECORD'S ID.
--     UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!" AND id = 2;

-- 7.) DELETE THE USER WITH AN EMAIL OF johndoe@gmail.com.
--     DELETE FROM users WHERE email = "johndoe@gmail.com";
